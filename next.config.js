module.exports = {
  env: {
    DB_HOST: "localhost:3000",
    primaryColor: "#1f1f1f",
    secondColor: "#389e0d",
  },
  experimental: {
    outputStandalone: true,
  },
};
