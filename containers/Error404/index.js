import React from "react";

const Error404 = () => {
  return (
    <div className="d-flex vw-100 vh-100 justify-content-center align-items-center bg-info">
      <h1> Error404: No encontrado.</h1>
    </div>
  );
};

export default Error404;
