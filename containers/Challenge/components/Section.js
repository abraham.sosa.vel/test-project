import React, { useState, useEffect } from "react";

const Section = () => {
  const [title, setfirst] = useState("");
  useEffect(() => {
    setfirst(typeof window === "undefined" ? " servidor" : " cliente");
  }, [title]);

  return (
    <div
      className="d-flex justify-content-center align-items-center bg-primary m-2"
      style={{ flex: "2 1 auto" }}
    >
      <p>{`del lado del${title}`}</p>
    </div>
  );
};

export default Section;
