import React from "react";

const Aside = () => {
  return (
    <div
      className="d-flex justify-content-center align-items-center bg-success m-2"
      style={{ flex: "1 1 auto" }}
    >
      Aside
    </div>
  );
};

export default Aside;
