import React from "react";

const Header = ({ title = "" }) => {
  return (
    <div
      className="d-flex justify-content-center align-items-center bg-secondary m-2"
      style={{ height: "4rem" }}
    >
      {title}
    </div>
  );
};

export default Header;
