import React from "react";
import Aside from "./components/Aside";
import Footer from "./components/Footer";
import Header from "./components/Header";
import Nav from "./components/Nav";
import Section from "./components/Section";

const Challenge = ({ data = "" }) => {
  return (
    <container
      className="d-flex flex-column vw-100 vh-100 m-auto flex-nowrap p-4 bg-info"
      style={{ maxWidth: "1200px" }}
    >
      <Header title={data} />
      <Nav />
      <div className="h-100 d-flex flex-column-reverse flex-md-row">
        <Aside />
        <Section />
      </div>
      <Footer />
    </container>
  );
};

export default Challenge;
