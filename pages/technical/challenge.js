import React from "react";
import Challenge from "../../containers/Challenge";

const ChallengePage = ({ data }) => {
  console.log(data);
  return <Challenge data={data.name} />;
};
ChallengePage.getInitialProps = (ctx) => {
  const url = `${process.env.DB_HOST}/api/header`;
  return fetch("localhost:3000/api/header")
    .then((res) => (res.ok ? res.json() : Promise.reject(res)))
    .then((json) => {
      return { data: json };
    })
    .catch((error) => {
      let text = error.statusText || "No se encontró la ruta";
      return { data: text };
    });
};

export default ChallengePage;
